var gulp  = require('gulp');
var gutil = require('gulp-util');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');

gulp.task('build-css', function() {
  return gulp.src('source/scss/**/*.scss')
  	.pipe(sourcemaps.init())
    .pipe(sass()
        .on('error',function(error) {
            gutil.log(error.messageOriginal);
            this.emit('end');
        })
    )
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('public/assets/stylesheets'));
});    

gulp.task('default', function() {
  return gutil.log('Gulp is running!')
});

gulp.task('watch', function() {
	gulp.watch('source/scss/**/*.scss', ['build-css']);
});



